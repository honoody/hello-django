from django.urls import path

from . import views

urlpatterns = [
    path("", views.LibraryView.as_view()),
    path("<slug:slug>/", views.ShelveView.as_view()),
    path("get/<int:pk>/", views.GetBook.as_view(), name='get_book'),
    path("put/<int:pk>/", views.PutBook.as_view(), name='put_book'),
]

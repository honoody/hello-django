from django.db import models


# Create your models here.

class Shelves(models.Model):
    """Полки"""
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Books(models.Model):
    """Книги"""
    id = models.BigAutoField(primary_key=True)
    shelve = models.ForeignKey(Shelves, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.TextField()
    author = models.CharField(max_length=20)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.name